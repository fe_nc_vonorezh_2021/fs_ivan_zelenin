let mass = [];
let numMass;
let summ;

for ( let i = 0; i < 101; i++ ) {
        numMass = Math.round( Math.random() * 100 );
        mass.push(numMass); 
};
console.log(mass);
mass.sort((a, b) => b - a);
console.log(mass);
summ = mass.filter(i => i%2);
summ = summ.map(i => Math.pow(i, 2));
summ = summ.reduce((a, b) => a + b);
console.log(summ);
