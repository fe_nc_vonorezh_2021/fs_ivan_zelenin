class ExzBook {
  constructor(year, people, date, invNum) {
    this.year = year
    this.people = people
    this.date = date
    this.invNum = invNum
  }
  set invNum(num) {
    if (num <= 0) {
      throw new Error("Неправильный инвентарный номер")   
    }
  }
};

class Book extends ExzBook {
  constructor(invNum, bookName, author, genre) {
    super(invNum)
    this.bookName = bookName
    this.author = author
    this.genre = genre
    delete this.date
    delete this.people
  }
};

class Library extends Book {
  constructor(bookName, listEx, author){
  super(bookName, author)
  this.author = author
  this.listEx = listEx
  delete this.genre
  delete this.year
  delete this.invNum
  };
  _nameLibrary = "Best Library"
  get _nameLibrary() {
    return this._nameLibrary;
  }
};

const testLibrary = new Library ("351 F", "25", "Ray Bradbury");

console.log(testLibrary);

